<?php
/*
Template Name: Home Template
*/
get_header();
?>

<main id="site-content" role="main">
	<div id="primary">
    <?php
	// Check value exists.
	if( have_rows('events_home') ):
	    // Loop through rows.
	    while ( have_rows('events_home') ) : the_row();
	    	switch (get_row_layout()) {
	    		case 'upcoming_event':
						get_template_part('template-parts/pages/home/content', 'upcoming-event');
	    			break;
	    	}
	    	  // End loop.
	    endwhile;
	// No value.
	else :
	    // Do something...
	endif;
	?>
	</div>
</main>
<?php get_footer(); ?>
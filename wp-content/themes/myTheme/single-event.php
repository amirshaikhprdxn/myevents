<?php
get_header();
?>
<section class="event">
	<div class="wrapper">
		<?php if( get_the_title() ): ?>
			<h3><?php echo get_the_title(); ?></h3>
		<?php endif; ?>
		<?php if( get_field('summary') ): ?>
			<p><?php echo get_field('summary'); ?></p>
		<?php endif; ?>
		<?php if( get_field('start_date_time') ): ?>
			<span>Event Start Date &amp; Time : <?php echo get_field('start_date_time'); ?></span>
		<?php endif; ?>
		<?php if( get_field('end_date_time') ): ?>
			<span>Event End Date &amp; Time : <?php echo get_field('end_date_time'); ?></span>
		<?php endif; ?>
	</div>
</section>
<?php
get_footer();
?>

<?php
/* Activate Custom Post Type */
function custom_post_type() {
	$labels = array(
		'name' => 'Event',
		'singular_name' => 'event',
		'add_new' => 'Add New',
		'all_items' => 'All Item',
		'add_new_item' => 'Add Item',
		'edit_item' => 'Edit Item',
		'new_item' => 'New Item',
		'view_item' => 'View Item',
		'search_item' => 'Search Event',
		'not_found' => 'No Items Found',
		'not_found_in_trash' => 'No Items Found In Trash',
		'parent_item_colon' => 'Parent Item'
	);
	$args = array (
		'labels' => $labels,
		'public' => true,
		'has_archive' => true,
		'publicaly_queryable' => true,
		'qurey_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'supports' => array(
			'title',
			'editor',
			'excerpt',
			'thumbnail',
			'revisions'
		),
		'taxonomies' => array(
			'category',
			'post_tag'
		),
		'menu_position' => 5,
		'exclude_from_search' => false
	);
	register_post_type('Event', $args);
}

add_action( 'init', 'custom_post_type' );

function more_post_ajax() {

    $option = (isset($_POST["name"])) ? $_POST["name"] : 'all';

    header("Content-Type: text/html");
    ?>
		<?php
		$args = array(
			'post_type'			=> 'event',
			'posts_per_page'	=> -1
		);
		$the_query = new WP_Query($args);
		?>
		<div class="wrapper">
		<?php if( $the_query->have_posts() ): ?>
			<ul class="upcoming-events">
			<?php while( $the_query->have_posts() ) : $the_query->the_post();
			    ?>
				<?php
					$curr_date = date('F j, Y g:i a');
					$pdate = get_field('start_date_time');
				?>

				<?php if ($option == 'all') : ?>
			    <li>
					<?php if( get_the_title( $post->ID ) ): ?>
						<h3><?php echo get_the_title( $post->ID ); ?></h3>
					<?php endif; ?>
					<?php if( get_field('start_date_time') ): ?>
						<span><?php echo get_field( 'start_date_time' ); ?></span>
					<?php endif; ?>
			    	<a class="see-more" href="<?php echo get_permalink( $post->ID ); ?>" title="See More">see more</a>
			    </li>
			    <?php endif; ?>

				<?php if ($option == 'upcoming') : ?>
					<?php if ($curr_date > $pdate) : ?>
					<li>
						<?php if( get_the_title( $post->ID ) ): ?>
							<h3><?php echo get_the_title( $post->ID ); ?></h3>
						<?php endif; ?>
						<?php if( get_field('start_date_time') ): ?>
							<span><?php echo get_field( 'start_date_time' ); ?></span>
						<?php endif; ?>
				    	<a class="see-more" href="<?php echo get_permalink( $post->ID ); ?>" title="See More">see more</a>
				    </li>
				    <?php endif; ?>
			    <?php endif; ?>

				<?php if ($option == 'past') : ?>
					<?php if ($curr_date < $pdate) : ?>
					<li>
						<?php if( get_the_title( $post->ID ) ): ?>
							<h3><?php echo get_the_title( $post->ID ); ?></h3>
						<?php endif; ?>
						<?php if( get_field('start_date_time') ): ?>
							<span><?php echo get_field( 'start_date_time' ); ?></span>
						<?php endif; ?>
				    	<a class="see-more" href="<?php echo get_permalink( $post->ID ); ?>" title="See More">see more</a>
				    </li>
				    <?php endif; ?>
			    <?php endif; ?>
			<?php endwhile; ?>
			</ul>
		<?php endif; ?>
		<?php wp_reset_query(); ?>
		</div>
    <?php
}

add_action('wp_ajax_nopriv_more_post_ajax', 'more_post_ajax');
add_action('wp_ajax_more_post_ajax', 'more_post_ajax');
?>
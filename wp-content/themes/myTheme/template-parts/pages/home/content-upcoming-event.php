<div class="wrapper">
	<form method="POST">
	    <label for="cat">Filter Events:</label>
	    <select class="cat" name="categories">
	        <option value="upcoming">Upcoming Events</option>
	        <option value="past">Past Events</option>
	        <option value="all">All Events</option>
	    </select>
	</form>
</div>

<section id="ajax-posts">
	<div class="wrapper">
		<?php if( get_sub_field('title') ): ?>
			<h2><?php echo get_sub_field('title'); ?></h2>
		<?php endif; ?>
		<?php
		$args = array(
			'post_type'			=> 'event',
			'posts_per_page'	=> -1
		);
		$the_query = new WP_Query($args);
		?>
		<?php if( $the_query->have_posts() ): ?>
			<ul class="upcoming-events">
			<?php while( $the_query->have_posts() ) : $the_query->the_post();
			    ?>
				<?php
					$curr_date = date('F j, Y g:i a');
					$pdate = get_field('start_date_time');
				?>
				<?php if ($curr_date > $pdate) : ?>
			    <li>
					<?php if( get_the_title( $post->ID ) ): ?>
						<h3><?php echo get_the_title( $post->ID ); ?></h3>
					<?php endif; ?>
					<?php if( get_field('start_date_time') ): ?>
						<span><?php echo get_field( 'start_date_time' ); ?></span>
					<?php endif; ?>
			    	<a class="see-more" href="<?php echo get_permalink( $post->ID ); ?>" title="See More">see more</a>
				</li>
			    <?php endif; ?>
			<?php endwhile; ?>
			</ul>
		<?php endif; ?>
		<?php wp_reset_query(); ?>
	</div>
</section>

<?php get_footer(); ?>
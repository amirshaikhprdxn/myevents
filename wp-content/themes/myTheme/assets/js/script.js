var selectedCategory;
jQuery(document).ready(function($) {
    $("select.cat").change(function() {
        selectedCategory = $(this).children("option:selected").val();
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "/wp-admin/admin-ajax.php",
            data: {
                    'post_type' : 'event',
                    name : selectedCategory,
                    'action': 'more_post_ajax'
                },
            success: function(data) {
                var $data = $(data);
                console.log($data);
                if ($data.length) {
                    $('#ajax-posts').empty();
                    $("#ajax-posts").append($data);
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
            }
        });
        return false;
    });
});